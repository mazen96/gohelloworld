package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
)

type operation struct {
	num1     float64
	num2     float64
	operator string
	result   string
}

func main() {

	for true {
		////////////////////// Initialization process ////////////////////////////////////
		fmt.Println("Welcome !")
		fmt.Println("Press 0 to exit, 1 for history, any number to continue")
		fmt.Print("Your Choice: ")

		var choice int = -1
		for true {
			_, err := fmt.Scanf("%d", &choice)
			if err == nil {
				break
			} else {
				fmt.Println("Invalid Input Please Enter a Valid Number")
			}
		}

		if choice == 0 {
			fmt.Println("Terminating ... GoodBye!")
			break
		}

		if choice == 1 {
			fmt.Println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
			fmt.Println("%%%%%%%%%%%%%history%%%%%%%%%%%%%%%%%%%%%")
			fmt.Println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
			readHistory()
			fmt.Println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
			fmt.Println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
			continue
		}

		///////////////////////////// Take user input /////////////////////////////////////////////////
		fmt.Print("Please enter first number : ")
		var num1 float64
		for true {
			_, err := fmt.Scanf("%f", &num1)
			if err == nil {
				break
			} else {
				fmt.Println("Invalid Input Please Enter a Valid Number")
			}
		}

		fmt.Print("Please enter second number : ")
		var num2 float64
		for true {
			_, err := fmt.Scanf("%f", &num2)
			if err == nil {
				break
			} else {
				fmt.Println("Invalid Input Please Enter a Valid Number")
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		/////////////////// Taking operator, calc result and save whole operation///////////////
		var newOperation operation
		newOperation.num1 = num1
		newOperation.num2 = num2

		var op string
		for true {
			fmt.Print("please select operation type + , - , * , / : ")
			_, _ = fmt.Scan(&op)
			if op == "+" || op == "-" || op == "*" || op == "/" {
				newOperation.operator = op
				break
			}
		}

		var result float64

		switch op {
		case "+":
			result = Sum(num1, num2)
			newOperation.result = fmt.Sprintf("%.1f", result)
			break
		case "-":
			result = Diff(num1, num2)
			newOperation.result = fmt.Sprintf("%.1f", result)
			break
		case "*":
			result = Mul(num1, num2)
			newOperation.result = fmt.Sprintf("%.1f", result)
			break
		case "/":
			var err error
			result, err = Div(num1, num2)
			if err != nil {
				// div by zero case
				newOperation.result = err.Error()
			}
			newOperation.result = fmt.Sprintf("%.1f", result)
			break

		} // end of switch

		var line string = ""
		line += fmt.Sprintf("%.1f", newOperation.num1) + " " + newOperation.operator + " "
		line += fmt.Sprintf("%.1f", newOperation.num2) + " = " + newOperation.result
		writeString(line)

		fmt.Println("Result == ", result)

	} // end of main loop

}

func Sum(x float64, y float64) float64 {
	z := x + y
	return z
}

func Diff(x float64, y float64) float64 {
	z := x - y
	return z
}

func Mul(x float64, y float64) float64 {
	z := x * y
	return z
}

func Div(x float64, y float64) (float64, error) {
	if y == 0 {
		return 0, errors.New("undefined ... divide by zero") // 0 won't be used on return used only as decoy
	}

	z := x / y
	return z, nil
}

func writeString(dataLine string) {
	f, err := os.OpenFile("history.txt",
		os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	if _, err := f.WriteString(dataLine + "\n"); err != nil {
		log.Println(err)
	}

}

func readHistory() {
	file, err := os.OpenFile("history.txt",
		os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	//os.Open("history.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}
